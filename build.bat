@echo off

rem Default variables
set MSBUILD=()
set _MSBUILD_TARGET=Build
set _MSBUILD_CONFIG=Debug
set _MSBUILD_PLATFORM=x64
set _MSBUILD_APPX_BUNDLE_PLATFORMS="x64|ARM64"



rem Add path to MSBuild Binaries
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe" (
    set MSBUILD="%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe" (
    set MSBUILD="%ProgramFiles%\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe" (
    set MSBUILD="%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe" (
    set MSBUILD="%ProgramFiles%\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe" (
	set MSBUILD="%ProgramFiles(x86)%\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe" (
	set MSBUILD="%ProgramFiles%\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Preview\MSBuild\Current\Bin\MSBuild.exe" (
    set MSBUILD="%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Preview\MSBuild\Current\Bin\MSBuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe" (
    set MSBUILD="%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe" (
    set MSBUILD="%ProgramFiles%\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\Microsoft Visual Studio\2022\Professional\MSBuild\Current\Bin\MSBuild.exe" (
    set MSBUILD="%ProgramFiles%\Microsoft Visual Studio\2022\Professional\MSBuild\Current\Bin\MSBuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\Microsoft Visual Studio\2022\Enterprise\MSBuild\Current\Bin\MSBuild.exe" (
    set MSBUILD="%ProgramFiles%\Microsoft Visual Studio\2022\Enterprise\MSBuild\Current\Bin\MSBuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles(x86)%\MSBuild\14.0\bin" (
    set MSBUILD="%ProgramFiles(x86)%\MSBuild\14.0\bin\msbuild.exe"
    goto :FOUND_MSBUILD
)
if exist "%ProgramFiles%\MSBuild\14.0\bin" (
    set MSBUILD="%ProgramFiles%\MSBuild\14.0\bin\msbuild.exe"
    goto :FOUND_MSBUILD
)



rem Check for Visual Studio / MSBuild
if %MSBUILD%==() (
    echo "I couldn't find MSBuild on your PC. Make sure it's installed somewhere, and if it's not in the above if statements (in build.bat), add it."
    goto :EXIT
) 



rem Command line argument loop
:FOUND_MSBUILD
if (%1) == () goto :POST_ARGS_LOOP
if (%1) == (clean) (
    goto :MSBUILD_CLEAN
)
if (%1) == (rel) (
    goto :MSBUILD_RELEASE
)
if (%1) == (release) (
    goto :MSBUILD_RELEASE
)
if (%1) == (x64) (
    goto :MSBUILD_AMD64
)
if (%1) == (amd64) (
    goto :MSBUILD_AMD64
)
if (%1) == (AMD64) (
    goto :MSBUILD_AMD64
)
if (%1) == (arm64) (
    goto :MSBUILD_AMD64
)
if (%1) == (ARM64) (
    goto :MSBUILD_AMD64
)

echo Unknown option: %1
goto :EXIT

:ARGS_LOOP
shift
goto :FOUND_MSBUILD



rem Functions for command line argument loop
:MSBUILD_CLEAN
    set _MSBUILD_TARGET=Clean,Build
goto :ARGS_LOOP

:MSBUILD_RELEASE
    set _MSBUILD_CONFIG=Release
goto :ARGS_LOOP

:MSBUILD_AMD64
    set _MSBUILD_PLATFORM=x64
    set _MSBUILD_APPX_BUNDLE_PLATFORMS=x64
goto :ARGS_LOOP

:MSBUILD_ARM64
    set _MSBUILD_PLATFORM=ARM64
    set _MSBUILD_APPX_BUNDLE_PLATFORMS=ARM64
goto :ARGS_LOOP



rem Build solution
:POST_ARGS_LOOP
echo [i] Target: %_MSBUILD_TARGET%
echo [i] Config: %_MSBUILD_CONFIG%
echo [i] Build Platform: %_MSBUILD_PLATFORM%
echo [i] Appx Platform : %_MSBUILD_APPX_BUNDLE_PLATFORMS%
%MSBUILD% %~dp0\DistroLauncher.sln /t:%_MSBUILD_TARGET% /m /nr:true ^
    /p:Configuration=%_MSBUILD_CONFIG% ^
    /p:Platform=%_MSBUILD_PLATFORM% ^
    /p:AppxBundlePlatforms=%_MSBUILD_APPX_BUNDLE_PLATFORMS% ^
    /p:UseSubFolderForOutputDirDuringMultiPlatformBuild=false

if (%ERRORLEVEL%) == (0) (
    echo.
    echo Created appx in: %~dp0AppPackages\DistroLauncher-Appx\
    echo.
)



rem End
:EXIT
